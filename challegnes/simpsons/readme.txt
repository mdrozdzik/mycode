For your morning challenge, attempt the following:

From the challenge list, pull the strings eyes, goggles, and nothing and create a print function that returns this output:
My eyes! The goggles do nothing!
From the trial list, pull the strings eyes, goggles, and nothing and create a print function that returns this output:
My eyes! The goggles do nothing!
From the nightmare list, pull the strings eyes, goggles, and nothing and create a print function that returns this output:
My eyes! The goggles do nothing!
You're not allowed to edit any of the lists :)

When your script runs, it should output the following:

My eyes! The goggles do nothing!
My eyes! The goggles do nothing!
My eyes! The goggles do nothing!
