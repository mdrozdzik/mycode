"""For your morning challenge, attempt the following:

From the challenge list, pull the strings eyes, goggles, and nothing and create a print function that returns this output:
My eyes! The goggles do nothing!
From the trial list, pull the strings eyes, goggles, and nothing and create a print function that returns this output:
My eyes! The goggles do nothing!
From the nightmare list, pull the strings eyes, goggles, and nothing and create a print function that returns this output:
My eyes! The goggles do nothing!
You're not allowed to edit any of the lists :)

When your script runs, it should output the following:

My eyes! The goggles do nothing!
My eyes! The goggles do nothing!
My eyes! The goggles do nothing!
"""



challenge= ["science", "turbo", ["goggles", "eyes"], "nothing"]

trial= ["science", "turbo", {"eyes": "goggles", "goggles": "eyes"}, "nothing"]

nightmare= [{"slappy": "a", "text": "b", "kumquat": "goggles", "user":{"awesome": "c", "name": {"first": "eyes", "last": "toes"}},"banana": 15, "d": "nothing"}]



a = challenge[2][1]
b = challenge[2][0]
c = challenge[-1]

print(f"my {a}! The {b} do {c}!")

a= trial[2]["goggles"]
b= trial[2]["eyes"]
c= trial[-1]

print(f"My {a}! The {b} do {c}!")

a= nightmare[0]["user"]["name"]["first"]
b= nightmare[0]["kumquat"]
c= nightmare[0]["d"]

print(f"My {a}! The {b} do {c}!")
